import http from "http";
import getSAMLSolver from "../solver/index";


export default function startServer(port, user, password, baseUrl, initPath = '/', optionals) {
    let samlProxy = getSAMLSolver(user, password, baseUrl, initPath, optionals);
    const server = http.createServer((req, res) => {
        samlProxy(req, res);
    });

    server.listen(port, () => {
        console.log(`SAML Solver proxy listening at http://localhost:${port}`)
    });
}

import urlLib from "url";
import https from "https";
import {EventEmitter} from 'events';
import {Logger} from './Logger';
import {AuthenticationError} from "./AuthenticationError";
import {ResponseStub} from "./ResponseStub";
import {SAMLAuthenticator} from './SAMLAuthenticator'

let log;

export class SAMLSolver {

    static _getOptionalDefaults() {
        return {
            debug: false, keepaliveFreq: 300000, urlBlacklist: [],
            manualInteractions: {}, pathMappings: {}, maxRetries: 10,
            allowUnsafeSSL: false,
            authConfig: {}
        }
    };

    /***
     * Instantiate the SAMLSolver
     * @param user the user used for authentication
     * @param password the password used for authentication
     * @param baseURL the url to proxy the requests to
     * @param initPath A url to use for saml token retrieval and keepalive requests
     * @param optionals An object of optional parameters:
     *  debug: whether or not to log debug messages
     *  keepaliveFreq: specifies the frequency of session keepalive request in ms (default: 300000)
     *  urlBlacklist: blacklist of urls to ignore failures on (during authentication)
     */
    constructor(user, password, baseURL, initPath, optionals = {}) {
        optionals = {...SAMLSolver._getOptionalDefaults(), ...optionals};

        this.debugModeEnabled = optionals.debug === true;
        log = new Logger(this.constructor.name, this.debugModeEnabled ? 'DEBUG' : 'WARN');

        this.baseURL = baseURL;
        this.initPath = initPath;
        this.pathMappings = optionals.pathMappings;
        this.maxRetries = optionals.maxRetries;
        this.urlBlacklist = optionals.urlBlacklist;
        this.allowUnsafeSSL = optionals.allowUnsafeSSL

        this.authenticator = new SAMLAuthenticator(
            user, password, this.baseURL, this.debugModeEnabled,
            optionals.authConfig, optionals.manualInteractions, optionals.urlBlacklist);
        let samlSolver = this;
        if( optionals.keepaliveFreq > 0 ) {
            setInterval(() => {
                samlSolver._keepalive.bind(samlSolver)()
                    .then(() => {})
                    .catch((err) => {
                        log.warn("Error during keepalive request!");
                        log.warn(err);
                    });
            }, optionals.keepaliveFreq)
        }

        log.debug("Loaded path mappings:");
        log.debug(this.pathMappings);

    }

    /***
     * Resolve a local request by proxying it to the SAML-enabled backend
     * @param req The local request as received by the nodeJS server
     * @param res The response returned by the nodeJS server
     * @param next Only pass if you intend to use the saml solver as a middle ware; in this case,
     *             the request is patched with the acquired cookies but not sent to the backend
     * @returns {Promise<void>}
     */
    async resolve(req, res, next) {

        log.debug(`PATH: ${req.url}`);
        log.debug("is_authenticated? " + (this.authenticator.isAuthenticated() ? "true" : "false"));

        try {
            await this.authenticator.ensureAuthenticated(this.baseURL, this.initPath);
        } catch(err) {
            SAMLSolver._handleHttpError(err, res);
            return;
        }

        if(next) {
            this.authenticator.patchRequest(req);
            next();
        } else {
            try {
                await this._performAuthenticatedRequest(this.baseURL, req.url + "", req, res);
            } catch(err) {
                if (res.statusCode >= 400 && res.statusCode < 600 )
                    log.warn("SAMLSOLVER:: ", `ERROR: ${res.statusCode} while requesting ${this.baseURL}/${req.url}!`);
                else
                    log.warn(`"SAMLSOLVER:: An error occured!\n${err}`);
            }
        }
    }

    static _handleHttpError(err, res) {
        if(err && err.constructor.name === 'AuthenticationError') {
            res.writeHead(err.httpStatus, {});
            res.write(err.message);
            res.end();
        }
        else {
            log.warn("SAMLSOLVER:: ", err);
            res.writeHead(500, {});
            res.write("Unexpected error!");
            res.end();
        }

    }

    async _keepalive() {
        if( !this.authenticator.isAuthenticated() ) {
            log.debug("We're not authenticated yet; skipping keepalive request...");
            return;
        }

        log.debug(`Performing keepalive request to ${this.baseURL}/${this.initPath}...`);

        let req, res;
        let requestUrl = this.baseURL;
        let requestPath = this.initPath;
        do {

            req = {
                headers: [],
                pipe: req => {
                    req.end();
                    return new EventEmitter();
                },
                [Symbol.iterator]: function*() {
                    yield Promise.resolve("");
                }
            };

            res = new ResponseStub();

            try {
                await this._performAuthenticatedRequest(requestUrl, requestPath, req, res, false);
            } catch( e ) {
                log.warn( `Error during SAMLSolver keepalive request:  ${e}`);
            }

            if(res.status === 302 || res.status === 301) {
                log.debug(`Following redirect to ${res.headers['location']}...`);
                let loc = urlLib.parse(res.headers['location']);
                requestUrl = loc.origin;
                requestPath = loc.pathname;
            }
        } while (res.status === 302 || res.status === 301);

        if(res.status >= 400 && res.status < 600)
            log.warn(`Keepalive request failed with Error ${res.status}!`);
        else
            log.debug("Keepalive request succeeded.")

    }

    async _performAuthenticatedRequest(origin, origPath, req, res, enablePathMapping = true) {
        log.debug("performing authenticated request...");

        let path = enablePathMapping ? this._applyPathMapping(origPath) : origPath;
        const urlObj = new URL(origin + path);
        const {
            hostname: host,
            href: url,
            port,
        } = urlObj;

        this.authenticator.patchRequest(req);

        let options = {
            host,
            url,
            path,
            port,
            headers: {
                ...req.headers,
                host
            },
            method: req.method,
            rejectUnauthorized: !this.allowUnsafeSSL,
        };

        if ( this.debugModeEnabled )
            // disable gzip compression in debug mode (because we have trouble reading it in the proxy)
            options.headers['accept-encoding'] = 'deflate';

        log.debug(`Sending request to ${options.url}, host: ${options.host}...`);

        let isRequestCompleted = false;
        let requestSucceeded = false;
        let retries = this.maxRetries;
        let remoteResponse;
        // Buffer request in order to be able to retry
        const buffer = [];
        for await (const chunk of req) {
            buffer.push(chunk);
        }
        let proxyRequest;
        //let forward = req.pipe(proxyRequest);

        let samlCookiePattern = /^JSESSIONID=[A-Z0-9]*;/;
        while( !requestSucceeded && retries > 0 ) {
            retries--;
            proxyRequest = https.request(options);
            for (const chunk of buffer) {
                proxyRequest.write(chunk);
            }
            proxyRequest.end();
            remoteResponse = await new Promise((resolve) => {
                proxyRequest.on('response', resolve);
            });
            if( 'set-cookie' in remoteResponse.headers
                && samlCookiePattern.test(remoteResponse.headers['set-cookie'])) {
                log.debug("Remote response headers:");
                log.debug(Object.keys(remoteResponse.headers).map(k => `${k}: ${remoteResponse.headers[k]}`));
                log.warn(`Server did not accept authentication. Retrying ${retries} more times...`);
                await proxyRequest.abort();
            } else {
                requestSucceeded = true;
            }
        }

        if (!requestSucceeded)
            throw new AuthenticationError("Error: Request failed!", 200, remoteResponse);

        // forward.on('finish', () => {
        //     debug('Forwarded request finished');
        //     // markRequestCompleted();
        //     isRequestCompleted = true;
        // });
        // forward.on('error', (err) => {
        //     console.warn("SAMLSOLVER:: ", err);
        //     // markRequestCompleted();
        //     isRequestCompleted = true;
        // });

        proxyRequest.on('finish', () => {
            log.debug('Forwarded request finished');
            // markRequestCompleted();
            isRequestCompleted = true;
        });
        proxyRequest.on('error', (err) => {
            log.warn("SAMLSOLVER:: ", err);
            // markRequestCompleted();
            isRequestCompleted = true;
        });

        log.debug("Request headers:");
        log.debug(Object.keys(req.headers).map(k => `${k}: ${req.headers[k]}`));
        log.debug('Forwarded headers:');
        log.debug(Object.keys(options.headers).map(k => `${k}: ${options.headers[k]}`));

        if( retries === 0 ) {
            let msg = `Could not resolve ${url}! Maximum retries exceeded`;
            log.error(msg);
            res.error(msg);
        }

        log.debug("Remote response headers:");
        log.debug(Object.keys(remoteResponse.headers).map(k => `${k}: ${remoteResponse.headers[k]}`));

        if(remoteResponse.statusCode === 301 || remoteResponse.statusCode === 302) {
            log.debug(`Forward (${remoteResponse.statusCode}) detected! Target: '${remoteResponse.headers['location']}'`);
            if(remoteResponse.headers.location.indexOf(host) !== -1) {
                log.debug(`Rewriting forwarding location ${remoteResponse.headers['location']}...`);
                remoteResponse.headers.location = remoteResponse.headers.location
                    .replace(host, req.headers['host'] || "http://localhost:3000")
                    .replace("https://", "http://");
                log.debug(`New location: ${remoteResponse.headers['location']}`);
            }
        }

        res.writeHead(remoteResponse.statusCode, remoteResponse.headers);
        log.debug("Response header:");
        if(res.headers)
            log.debug(Object.keys(res.headers).map(k => `${k}: ${res.headers[k]}`));

        if( remoteResponse.statusCode !== 200 )
            log.debug(`Received status code ${remoteResponse.statusCode}`);

        remoteResponse.on('data', (data) => {
            res.write(data);
        });

        await new Promise((resolve, reject) => {
            remoteResponse.on('end', () => {
                res.end();
                // if (!isRequestCompleted) {
                //     console.warn('SAMLSOLVER:: Warning: Server response ended, before request could be completely forwarded!');
                // }
                if (remoteResponse.statusCode >= 400 && remoteResponse.statusCode <= 600)
                    log.warn(`SAMLSOLVER:: Warning: Received ${remoteResponse.statusCode} while requesting ${url}!`,
                        ` (original url: ${req.headers['host']}${req.url}, path: ${path})`);
                log.debug('response status: ' + remoteResponse.statusCode);

                resolve();
            });

            remoteResponse.on('error', reject);
        });

            // let markRequestCompleted = null;
            // let requestComplete = new Promise((resolve) => {
            //     markRequestCompleted = resolve;
            // });

    }

    _applyPathMapping(path) {

        log.debug(`Apply path mappings on ${path}`);

        let newPath = path;
        for( let [mKey, mVal] of Object.entries(this.pathMappings) ) {
            if( path.startsWith(mKey) ) {
                newPath = path.replace(mKey, mVal);
                log.debug(`Replacing path: '${path}' -> '${newPath}'`);
            } else {
                log.debug(`mapping '${mKey}' does not match '${path}', ignoring...`)
            }
        }

        return newPath;
    }
}

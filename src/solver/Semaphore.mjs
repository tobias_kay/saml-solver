export class Semaphore {

    constructor(count = 0) {
        this.count = count;
        this.promise = Promise.resolve();
        this.resolve = () => {};
        this.reject = () => {};
    }

    _createNewPromise() {
        const semaphore = this;
        this.promise = new Promise((resolve, reject) => {
            semaphore.resolve = resolve;
            semaphore.reject = reject;
        });
    }

    increase() {
        if(this.count === 0)
            this._createNewPromise();
        this.count++;
    }

    decrease() {
        if(this.count === 0) {
            this.reject();
            throw Error("Semaphore can't be decreased beyond 0!");
        }
        this.count--;
        if(this.count === 0)
            this.resolve();
    }

    async wait(timeout = undefined) {
        if(timeout)
            setTimeout(() => {
                    this.reject(new Error("Semaphore timed out!"));
                }, timeout);
        return this.promise;
    }
}

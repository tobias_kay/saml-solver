import {EventEmitter} from 'events';
import {StringDecoder} from 'string_decoder';

export class ResponseStub
{
    constructor(debug = false) {
        this.eventLog = [];
        this.emitter = new EventEmitter();
        this.status = undefined;

        this.on = this.emitter.on;
        this.emit = this.emitter.emit;

        this.headers = {};
        this.streamDecoder = new StringDecoder('utf8');

        this.dataBuffer = [];

        this.on('end', () => {
            if(debug) {
                console.log("Response finished. Received data:");
                this.dataBuffer.forEach((chunk) => {
                    console.log(this.streamDecoder.write(chunk));
                })
            }
        });
    }

    writeHead(status, headers) {
        this.status = status;
        this.headers = headers;
    };

    once(eventId) {
        if(this.eventLog.indexOf(eventId) === -1) {
            this.emit(eventId);
            this.eventLog.push(eventId);
        }
    }

    end() {
        this.emit('end');
    }

    write(data) {
        this.dataBuffer.push(data);
    }

}

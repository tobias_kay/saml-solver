import config from "../config.json";

import {startSAMLSolverServer} from '../src/index';


startSAMLSolverServer(3000, config.user, config.password, config.baseUrl, config.initPath,
    {
        authConfig: config.authConfig || {},
        urlBlacklist: config.urlBlacklist || [],
        manualInteractions: config.manualInteractions || {},
        pathMappings: config.pathMappings || {},
        keepaliveFreq: 30000,
        debug: true,
        allowUnsafeSSL: false
    });

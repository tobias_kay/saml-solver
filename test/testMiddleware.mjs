import config from '../config';
import http from 'http';
import {getSAMLSolverProxy} from '../src/index';

let port = 3000

let proxy = getSAMLSolverProxy(config.user, config.password, config.baseUrl, config.initPath,
    {
        authConfig: config.authConfig || {},
        urlBlacklist: config.urlBlacklist || [],
        manualInteractions: config.manualInteractions || {},
        pathMappings: config.pathMappings || {},
        keepaliveFreq: 30000,
        debug: true,
        allowUnsafeSSL: false
    });

function next(req, res) {
    console.log(req);
    console.log(res);
    console.log(next);
    res.writeHead(200, {'Content-Type': 'text/plain'})
    res.write('Hello world')
    res.end()
}

// let result = proxy(req, res, next);

const server = http.createServer((req, res) => {
    proxy(req, res, next.bind(this, req, res));
});

server.listen(port, () => {
    console.log(`SAML Solver Test proxy listening at http://localhost:${port}`)
});

# SAML Solver

## Description:

This package provides a proxy/middleware which forwards local requests to a backend that requires SAML authentication,
while dynamically retrieving an authentication token and passing it alongside the request.

Designed and tested for usage with SCP odata services.  
